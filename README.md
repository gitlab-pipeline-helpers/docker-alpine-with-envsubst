## docker-alpine-with-envsubst


### Pulling image

```
registry.gitlab.com/gitlab-pipeline-helpers/docker-alpine-with-envsubst:latest
```

### Creating tag that invokes Gitlab Pipeline

```
newtag=v1.0.1
git commit -a -m "changes for new tag $newtag" && git push -o ci.skip && git tag $newtag && git push origin $newtag
```

### Deleting tag

```
# delete local tag, then remote
todel=v1.0.1
git tag -d $todel && git push origin :refs/tags/$todel
